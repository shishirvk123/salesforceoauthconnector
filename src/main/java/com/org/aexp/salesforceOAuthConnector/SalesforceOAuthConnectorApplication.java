package com.org.aexp.salesforceOAuthConnector;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.aexp.salesforceOAuthConnector.service.Force;

@SpringBootApplication
@EnableOAuth2Sso
@RestController
public class SalesforceOAuthConnectorApplication extends WebSecurityConfigurerAdapter {

	@Autowired
	Force force;
	
	@RequestMapping("/user")
	public Principal user(Principal principal) {
	   return principal;
	}
	
	@RequestMapping("/accounts")
	  public List<Force.Account> accounts(OAuth2Authentication principal) {
		
		return force.accounts(principal);
	  }
	
	
	public static void main(String[] args) {
		SpringApplication.run(SalesforceOAuthConnectorApplication.class, args);
	}
	
	@Override
	  protected void configure(HttpSecurity http) throws Exception {
	    http
	      .antMatcher("/**")
	      .authorizeRequests()
	        .antMatchers("/", "/login**", "/webjars/**", "/error**")
	        .permitAll()
	      .anyRequest()
	        .authenticated();
	  }
}
